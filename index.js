// console.log('Hello World')

// [SECTION] While Loop 
/*
	- a while loop takes in an expression/condition
	- expressions are ant unit of code that can be evaluated to a value
	- if the condition evaluates to true, the statement inside the block will be executed
*/

let count = 5;

while(count!==0){
	console.log('Count:', count);
	count--; // decrements count value
}

// [SECTION] Do while

/*let number = Number(prompt('Give me a number: '))

do {
	console.log('Number: ', number);
	number += 1; // increments value into 1
} while(number < 10);
*/
// [SECTION] For loop
	// initialization // condtition // change of value = increment
for(let count = 10; count <=20; count++){
	console.log(count);
}

let myString = 'tupe';
console.log('mySting length: ', myString.length);

console.log(myString[0]);
console.log(myString[1]);
console.log(myString[2]);
console.log(myString[3]);

for(i=0; i < myString.length; i++){
	console.log(myString[i]);
}

let myName = 'AlEx';
let vowelCount = 0;
for(let i=0; i<myName.length; i++){
    if(
        myName[i].toLowerCase() == 'a' ||
        myName[i].toLowerCase() == 'e' ||
        myName[i].toLowerCase() == 'i' ||
        myName[i].toLowerCase() == 'o' ||
        myName[i].toLowerCase() == 'u' 
    ){
        console.log(3);
        vowelCount ++;
    }
    else{
        console.log(myName[i]);
    }
}

console.log("Vowels: ", vowelCount);

